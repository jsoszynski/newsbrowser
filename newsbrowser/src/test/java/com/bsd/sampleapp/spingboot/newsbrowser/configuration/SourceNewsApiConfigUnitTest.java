package com.bsd.sampleapp.spingboot.newsbrowser.configuration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class SourceNewsApiConfigUnitTest {
	
	private SourceNewsApiConfig config;
	
	@TestConfiguration
    static class SourceNewsApiConfigUnitTestContextConfiguration {
		@Bean
		public Validator initValidator() {
			ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
			return factory.getValidator();
		}
	}
	
	@Autowired
	private Validator validator;
	
	@Before 
	public void setUp() {
		config = new SourceNewsApiConfig();
	}
	
	@Test
	public void shouldConfigBeValid() {
		config.setUrl("newsapi.org");
		config.setKey("111111111");
		assertTrue( validator.validate(config).isEmpty() );
	}
	
	@Test
	public void shouldConfigBeNotValidx() {
		assertFalse( validator.validate(config).isEmpty() );
	}
	
	@Test
	public void shouldUrlConfiBeNotValid() {
		config.setUrl(null);
		config.setKey("111111111");
	    assertFalse( validator.validate(config).isEmpty() );
	}
	
	@Test
	public void shouldKeyConfigBeNotValid() {
		config.setUrl("newsapi.org");
		config.setKey(null);
		assertFalse( validator.validate(config).isEmpty() );
	}

}
