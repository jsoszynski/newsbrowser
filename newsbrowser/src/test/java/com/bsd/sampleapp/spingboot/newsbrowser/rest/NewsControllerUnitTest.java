package com.bsd.sampleapp.spingboot.newsbrowser.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.bsd.sampleapp.spingboot.newsbrowser.service.NewsService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = NewsController.class)
public class NewsControllerUnitTest {
	
	@Autowired
    private MockMvc mvc;
	
	@MockBean
	NewsService newsService;
	
	private static final String PATH = "/news/";

	@Test
	public void shouldReceiveOkStatusForGetWithCorrectUrlAndParams() throws Exception {
		mvc.perform( get(PATH+"anyLanguage/anyCategory/") ).andExpect( status().isOk() );
	}
	
	@Test
	public void shouldNotBeMappedForGetWithoutSecodParam() throws Exception {
		mvc.perform( get(PATH+"anyLanguage/") ).andExpect( status().isNotFound() );
	}
	
	@Test
	public void shouldNotBeMappedForGetWithoutAnyParam() throws Exception {
		mvc.perform( get(PATH) ).andExpect( status().isNotFound() );
	}
	
	@Test
	public void shouldNotFoundMappingForPost() throws Exception {
		mvc.perform( post("/anyUrl") ).andExpect( status().isNotFound() );
	}

}
