package com.bsd.sampleapp.spingboot.newsbrowser.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bsd.sampleapp.spingboot.newsbrowser.configuration.SourceNewsApiConfig;
import com.bsd.sampleapp.spingboot.newsbrowser.other.UrlParameters;

@RunWith(SpringRunner.class)
public class UrlServiceUnitTest {
	
	@TestConfiguration
	public static class UrlServiceUnitTestContextConfiguration {
		@Bean
		public UrlService initUrlService() {
			return new UrlServiceImpl();
		}
	}
	
	@Autowired
	UrlService urlService;
	
	@MockBean
	SourceNewsApiConfig config;
	
	@Test
	public final void shouldGetUrlWithoutPageAndpageSize() {
		UrlParameters params = UrlParameters.builder("anyCountry", "anyCategory").build();
		
		when( config.getUrl() ).thenReturn( "http://anyUrl");
		when( config.getKey() ).thenReturn( "anyKey");
		
		assertThat( urlService.getFullUrl(params) )
		.contains("country=anyCountry")
		.contains("category=anyCategory")
		.contains("apiKey=anyKey")
		.doesNotContain("page=")
		.doesNotContain("pageSize=");
	}
	
	@Test
	public final void shouldGetUrlWithPageAndPageSize() {
		UrlParameters params = UrlParameters.builder("anyCountry", "anyCategory").page(2).pageSize(30).build();
		
		when( config.getUrl() ).thenReturn( "http://anyUrl");
		when( config.getKey() ).thenReturn( "anyKey");
		
		assertThat( urlService.getFullUrl(params) )
		.contains("country=anyCountry")
		.contains("category=anyCategory")
		.contains("apiKey=anyKey")
		.contains("page=2")
		.contains("pageSize=30");
	}

	@Test
	public final void shouldGetUrlWithoutPage() {
		UrlParameters params = UrlParameters.builder("anyCountry", "anyCategory").pageSize(30).build();
		
		when( config.getUrl() ).thenReturn( "http://anyUrl");
		when( config.getKey() ).thenReturn( "anyKey");
		
		assertThat( urlService.getFullUrl(params) )
		.contains("country=anyCountry")
		.contains("category=anyCategory")
		.contains("apiKey=anyKey")
		.doesNotContain("page=")
		.contains("pageSize=30");
	}
	
	@Test
	public final void shouldGetUrlWithoutPageSize() {
		UrlParameters params = UrlParameters.builder("anyCountry", "anyCategory").page(2).build();
		
		when( config.getUrl() ).thenReturn( "http://anyUrl");
		when( config.getKey() ).thenReturn( "anyKey");
		
		assertThat( urlService.getFullUrl(params) )
		.contains("country=anyCountry")
		.contains("category=anyCategory")
		.contains("apiKey=anyKey")
		.contains("page=2")
		.doesNotContain("pageSize=");
	}
	
}
