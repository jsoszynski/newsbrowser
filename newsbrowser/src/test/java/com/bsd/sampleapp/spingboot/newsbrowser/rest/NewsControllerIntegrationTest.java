package com.bsd.sampleapp.spingboot.newsbrowser.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import com.bsd.sampleapp.spingboot.newsbrowser.configuration.SourceNewsApiConfig;
import com.bsd.sampleapp.spingboot.newsbrowser.service.NewsService;
import com.bsd.sampleapp.spingboot.newsbrowser.service.NewsServiceImpl;
import com.bsd.sampleapp.spingboot.newsbrowser.service.UrlService;
import com.bsd.sampleapp.spingboot.newsbrowser.service.UrlServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(value = NewsController.class)
public class NewsControllerIntegrationTest {
	
	private static final String PATH = "/news/";
	
	@TestConfiguration
    static class NewsControllerIntegrationTestContextConfiguration {
		@Bean
		public SourceNewsApiConfig initSourceApi() {
			return new SourceNewsApiConfig();
		}
		
		@Bean RestTemplate initRestTemplate() {
			return new RestTemplate();
		}
		
        @Bean
        public NewsService initNewsService() {
            return new NewsServiceImpl();
        }
        
        @Bean
        public UrlService initUrlService() {
            return new UrlServiceImpl();
        }
    }
	
	@Autowired
    private MockMvc mvc;
	
	@Test
	// Assumtion : minimum one article of selected language and category is published at the moment.
	public void shouldGetSomeNumberNews() throws Exception {
		mvc.perform( get(PATH+"pl/technology/") )
		.andExpect( status().isOk() )
		.andExpect( jsonPath("[?(@.articles.length()>0)]").exists() );
	}
	
	@Test
	public void shouldGetZeroNewsWhenWrongCategory() throws Exception {
		mvc.perform( get(PATH+"pl/notExistedCategory/") )
		.andExpect( status().isOk() )
		.andExpect( jsonPath("[?(@.articles.length()==0)]").exists() );
	}
	
	@Test
	public void shouldGetZeroNewsWhenWrongLanguage() throws Exception {
		mvc.perform( get(PATH+"notExistedLanguage/technology/") )
		.andExpect( status().isOk() )
		.andExpect( jsonPath("[?(@.articles.length()==0)]").exists() );
	}

}
