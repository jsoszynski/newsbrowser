package com.bsd.sampleapp.spingboot.newsbrowser.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.bsd.sampleapp.spingboot.newsbrowser.configuration.SourceNewsApiConfig;
import com.bsd.sampleapp.spingboot.newsbrowser.dto.ResponseDto;

@RunWith(SpringRunner.class)
public class NewsServiceUnitTest {
	
	@TestConfiguration
	public static class NewsServiceUnitTestContextConfiguration {
		@Bean
		public NewsService initNewsService() {
			return new NewsServiceImpl();
		}
	}
	
	@Autowired
	NewsService newsService;
	
	@MockBean
	SourceNewsApiConfig sourceApi;
	
	@MockBean
	RestTemplate restTemplate;
	
	@MockBean
	UrlService urlService;

	@Test
	public void shouldResturnOkStatus() {
		when( urlService.getFullUrl(any()) ).thenReturn( "http://anyUrl" );
		when( restTemplate.getForEntity( anyString(), any() )).thenReturn( ResponseEntity.ok(new ResponseDto()) );
		
		ResponseEntity<?> result = newsService.getNews("anyLanguage", "anyTechnology", 0, 0);
		assertEquals( HttpStatus.OK, result.getStatusCode() );
	}

}
