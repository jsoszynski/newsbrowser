package com.bsd.sampleapp.spingboot.newsbrowser.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.bsd.sampleapp.spingboot.newsbrowser.configuration.SourceNewsApiConfig;
import com.bsd.sampleapp.spingboot.newsbrowser.other.UrlParameters;

@Service
public class UrlServiceImpl implements UrlService {
	
	@Autowired
	private SourceNewsApiConfig sourceApi;

	@Override
	public String getFullUrl(UrlParameters url) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(sourceApi.getUrl())
			.queryParam("country", url.getCountry())
			.queryParam("category", url.getCategory());
		if (url.getPageSize()>0) {
			uriBuilder.queryParam("pageSize", url.getPageSize());
		}
		if (url.getPage()>0) {
			uriBuilder.queryParam("page", url.getPage());
		}
		uriBuilder.queryParam("apiKey", sourceApi.getKey());
				
		return uriBuilder.toUriString();
	}

}
