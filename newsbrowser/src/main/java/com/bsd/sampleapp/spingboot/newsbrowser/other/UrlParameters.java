package com.bsd.sampleapp.spingboot.newsbrowser.other;

import javax.validation.constraints.NotBlank;

import lombok.Builder;
import lombok.Getter;

/**
 * Store url source API parameters
 * 
 *  @param country shortcut of country (mandatory)
 *  @param category category of news (mandatory)
 *  @param pageSize number of news to get by one request (optional)
 *  @param page number of news group whereas one group include news number defined by {@link pageSize} param (optional)
 * 
 * @author JS 2018-12-06
 */
@Builder(builderMethodName = "customBuilder")
@Getter
public class UrlParameters {
	
	@NotBlank private final String country;
	
	@NotBlank private final String category;
	
	@Builder.Default private int pageSize = 0;
	
	@Builder.Default private int page = 0;
	
	public static UrlParametersBuilder builder(String country, String category) {
		return customBuilder().country(country).category(category);
	}
	
}
