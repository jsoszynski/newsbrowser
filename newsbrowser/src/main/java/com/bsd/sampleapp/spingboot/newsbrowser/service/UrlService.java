package com.bsd.sampleapp.spingboot.newsbrowser.service;

import com.bsd.sampleapp.spingboot.newsbrowser.other.UrlParameters;

/**
 * Make service of source API url.
 * 
 * @author JS 2018-12-07
 *
 */
public interface UrlService {
	
	/**
	 * Get full url with variable and constant parameters.
	 * 
	 * @param parameters variable parameters to add to url
	 * @return full url
	 */
	public String getFullUrl(UrlParameters parameters);

}
