package com.bsd.sampleapp.spingboot.newsbrowser.service;

import org.springframework.http.ResponseEntity;

import com.bsd.sampleapp.spingboot.newsbrowser.dto.ResponseDto;

/**
 * Service to operate with news.
 * 
 * @author JS 2018-11-30
 * 
 */
public interface NewsService {

	ResponseEntity<ResponseDto> getNews(String country, String category, int page, int pageSize);

}
