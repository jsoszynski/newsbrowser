package com.bsd.sampleapp.spingboot.newsbrowser.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bsd.sampleapp.spingboot.newsbrowser.configuration.SourceNewsApiConfig;
import com.bsd.sampleapp.spingboot.newsbrowser.dto.ResponseDto;
import com.bsd.sampleapp.spingboot.newsbrowser.other.UrlParameters;

@Service
public class NewsServiceImpl implements NewsService {
	
	@Autowired
	SourceNewsApiConfig sourceApi;
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	   return builder.build();
	}
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	UrlService urlService;

	@Override
	public ResponseEntity<ResponseDto> getNews(String country, String category, int page, int pageSize) {
		UrlParameters urlParams = UrlParameters.builder(country, category).page(page).pageSize(pageSize).build();
		ResponseEntity<ResponseDto> response = restTemplate.getForEntity( urlService.getFullUrl(urlParams), ResponseDto.class );
		ResponseDto body = response.getBody();
		body.setCategory(category);
		body.setCountry(country);
		
		return ResponseEntity.ok(body);
	}

}
