package com.bsd.sampleapp.spingboot.newsbrowser.configuration;

import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

/**
 * Settings necessary to connect to source news API.
 * 
 * @author JS 2018-11-39
 *
 */
@Configuration
@ConfigurationProperties(prefix = "source.news.api")
@Validated
public class SourceNewsApiConfig {
	
	@NotBlank
	private String url;

	@NotBlank
	private String key;
	
	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getKey() {
		return key;
	}
	
}
