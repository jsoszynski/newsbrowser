package com.bsd.sampleapp.spingboot.newsbrowser.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import springfox.documentation.annotations.ApiIgnore;

/**
 * Redirect default context path to default view.
 * 
 * @author JS 2018-12-10
 *
 */
@Controller
@ApiIgnore
public class RedirectController {
     
    @GetMapping("/")
    public ModelAndView redirectToHomeView(ModelMap model) {
        return new ModelAndView("redirect:/news", model);
    }
    
}
