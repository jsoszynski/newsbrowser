package com.bsd.sampleapp.spingboot.newsbrowser.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import lombok.Data;

@Data
public class ResponseDto implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String country;
	
	private String category;
	
	private Set<ArticleDto> articles = new HashSet<>();

}
