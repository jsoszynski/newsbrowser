package com.bsd.sampleapp.spingboot.newsbrowser.other;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.bsd.sampleapp.spingboot.newsbrowser.dto.ArticleDto;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 *  Convert source API response to target API response format.
 * 
 * @author JS 2018-12-04
 */
public class ArticleDtoDeserializer extends StdDeserializer<ArticleDto> {
	private static final long serialVersionUID = 1L;

	public ArticleDtoDeserializer() {
        this(null);
    }
 
    public ArticleDtoDeserializer(Class<?> vc) {
        super(vc);
    }
 
    @Override
    public ArticleDto deserialize(JsonParser jp, DeserializationContext ctxt) 
      throws IOException {
    	JsonNode node = jp.getCodec().readTree(jp);
    	ArticleDto dto = new ArticleDto();
    	dto.setAuthor( getTextValue(node.get("author")) );
    	dto.setTitle( getTextValue(node.get("title")) );
    	dto.setDescription( getTextValue(node.get("description")) );
    	dto.setSourceName( getTextValue(node.get("source").get("name")) );
    	dto.setArticleUrl( getTextValue(node.get("url")) );
    	dto.setImageUrl( getTextValue(node.get("urlToImage")) );
    	String date = getTextValue(node.get("publishedAt"));
    	dto.setDate( LocalDate.parse(date, DateTimeFormatter.ISO_DATE_TIME) );
    	
		return dto;
    }
    
    private String getTextValue(JsonNode value) {
    	return value.isNull() ? "" : value.textValue();
    }

}
