package com.bsd.sampleapp.spingboot.newsbrowser.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bsd.sampleapp.spingboot.newsbrowser.dto.ResponseDto;
import com.bsd.sampleapp.spingboot.newsbrowser.service.NewsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Controller to handle news requests.
 * 
 * @author JS 2018-11-30
 *
 */
@RestController
@RequestMapping(path="news")
@Api(value="Controller to handle news requests.")
public class NewsController {
	
	@Autowired
	NewsService newsService;
	
	/**
	 * Get news articles from <a href="https://newsapi.org/docs/endpoints/top-headlines">newsapi.org</a> following below criteria's.
	 * Available param values are there defined.
	 * 
	 * @param country shortcut of country ex. 'pl', 'de'
	 * @param category name of news category
	 * @param page number of page to get
	 * @param pageSize number of news on one page
	 * @return list of news articles
	 * 
	 * @see <a href="https://newsapi.org/docs/endpoints/top-headlines">newsapi.org</a>
	 */
	@GetMapping(path="/{country}/{category}/")
	@ApiOperation(value = "Get news articles.", notes = "Get news articles from newsapi.org.", response = ResponseDto.class)
	public ResponseEntity<ResponseDto> getNews(
				@ApiParam(required = true, name = "country", value = "shortcut of country ex. 'pl', 'de'")
				@PathVariable(name="country") String country, 
				@ApiParam(required = true, name = "category", value = "name of news category")
				@PathVariable(name="category") String category,
				//WORKAROUND, skipped springfox @ApiParam due to known bug: NumberFormatException of Integer defaultValue.
				@RequestParam(value="page", required=false, defaultValue="0") Integer page,
				//WORKAROUND, the same as above
				@RequestParam(value="pageSize", required=false, defaultValue="0") Integer pageSize) {
		
		return newsService.getNews( country, category, page, pageSize );
	}

}
