package com.bsd.sampleapp.spingboot.newsbrowser.dto;

import java.io.Serializable;
import java.time.LocalDate;

import com.bsd.sampleapp.spingboot.newsbrowser.other.ArticleDtoDeserializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

@Data
@JsonDeserialize(using = ArticleDtoDeserializer.class)
public class ArticleDto implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String author;
	
	private String title;
	
	private String description;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate date;
	
	private String sourceName;
	
	private String articleUrl;
	
	private String imageUrl;

}
