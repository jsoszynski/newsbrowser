/**
 * Get news from source API and add to view table.
 */
$( document ).ready(function() {
	
    getData();
    
    $("#btnOk").click(function(event){
    	$("#footer").empty();
    	var urlParams = toUrlParams( $("#page").val(), $("#pageSize").val() );
    	getData(urlParams);
	})
	
    function getData(params) {   	
    	$.ajax({
            type : "GET",
            url : window.location + "/pl/technology/",
            data : params,
            success: function(data){
            	$('#browserDesc').html("source: news.api.org, " + "country: " + data.country + ", category: " + data.category);
                fillTable(data.articles);
            },
            error : function(e) {
            	$("#footer").html('<p class="bg-danger text-white mx-auto">Source data can not be found, now.</p>');
    			console.log("ERROR: ", e);
            }
        }); 
    }
    
	function fillTable(data){
		$('#newsTable tbody').empty();
		$.each(data, function(i, article){
			var row = buildRow(article);
			$('#newsTable tbody').append(row);
        });
	}
	
	function toUrlParams(page, pageSize) {
		return { 
			"page" : page,
			"pageSize" : pageSize
		};
	}
	
})