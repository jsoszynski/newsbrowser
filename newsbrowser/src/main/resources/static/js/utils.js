/**
 * Build row with one news information.
 */
function buildRow(article){
	
	return 	'<tr>' + 
				'<td>' + article.date + '</td>' + 
				'<td>' + article.sourceName + '</td>' + 
				'<td>' + article.author + '</td>' + 
				'<td>' + article.title + '</td>' + 
				'<td>' + article.description + '</td>' + 
				'<td>' + buildLink(article.articleUrl, "article") + '</td>' +
				'<td>' + buildLink(article.imageUrl, "image") + '</td>' +
			'</tr>';
}

function buildLink(url, text) {
	return '<a href="' +url + '">' + text + '</a>';
}