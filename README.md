##NewsBrowser

Simple application to browse one category local news from newsapi.org

** Tech stack :**
Java 8, Spring Boot 2.1.1 (with embedded Tomcat), Lombok, Springfox, Bootstrap 4.1, Maven
---

## How to run

This application is packaged as a war which has Tomcat embedded. No Tomcat installation is necessary.
To run application follow below steps.

1. Clone this repository.
2. Run by one of two options : 
    a) using **./mvnw spring-boot:run**
    or
    b) using **./mvnw clean package** to run a test and build WAR file and than **java -jar target/newsbrowser-0.0.1.war** to run.
3. Open **{host}:{port}/newsbrowser/news** url in your browser (ex. http://localhost:8080/newsbrowser/news) to browse received news.

---

## API

To get news by REST API use endpoint: **GET /news/{country}/{category}/**

To view detailed Swagger 2 API docs, run application and put url **{host}:{port}/newsbrowser/swagger-ui.html#/** (ex. http://localhost:8080/newsbrowser/swagger-ui.html#/ to your browser.
